import 'package:flutter/material.dart';
import 'package:flutter_begin/ProductListData.dart';
import 'package:shared_preferences/shared_preferences.dart';

List<ProductListData> data = [
  ProductListData(id: 1, name: 'Банан', desc: 'Спелый банан', price: 40.0, image: 'assets/images/banan.jpg', onBasket: false),
  ProductListData(id: 2, name: 'Груша', desc: 'Желтая груша', price: 53.0, image: 'assets/images/grusha.jpg', onBasket: false),
  ProductListData(id: 3, name: 'Апельсин', desc: 'Вкусный апельсин', price: 33.0, image: 'assets/images/6XZSr6ddCl6cxfo0UchP.jpg', onBasket: false),
  ProductListData(id: 4, name: 'Мандарин', desc: 'Сладкий мандарин', price: 50.0, image: 'assets/images/mandarin.jpg', onBasket: false),
  ProductListData(id: 5, name: 'Виноград', desc: 'Виноград с косточкой', price: 45.2, image: 'assets/images/dSTg1Wk44PJACMVYH1Z5.jpg', onBasket: false),
  ProductListData(id: 6, name: 'Киви', desc: 'Кислый киви', price: 60.4, image: 'assets/images/114963_900.jpg', onBasket: false),
];

class ProductList extends StatefulWidget{
  
  @override
  createState() => ProductListState();
}

class ProductListState extends State<ProductList>{
  
  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        title: Text('Список продуктов'),
        actions: <Widget>[
          Builder(
            builder: (BuildContext context){
              return IconButton(
                icon: Icon(Icons.shopping_basket),
                onPressed: (){Navigator.pushNamed(context, '/basket');},
              );
            },
          )
        ],
      ),
      body:  ProductListBuild(),
    );

  }
}

  class ProductListBuild extends StatefulWidget {
    loadProuctIDInBusket() async{
      final prefs = await SharedPreferences.getInstance();
      List<String> productIDsStorage = prefs.getStringList('productsIDInBasket') ?? [];
      data.forEach((p) => p.onBasket = false);

      if(productIDsStorage.length > 0 ){
        productIDsStorage.forEach((prod) {
          var product = data.firstWhere((p) => p.id.toString() == prod);
          product.onBasket = true;
        });
      }
    }
    @override
    createState() => ProductListBuildState();
  }

  class ProductListBuildState extends State<ProductListBuild> {
    loadProuctIDInBusket() async{
      final prefs = await SharedPreferences.getInstance();
      List<String> productIDsStorage = prefs.getStringList('productsIDInBasket') ?? [];
      data.forEach((p) => p.onBasket = false);

      if(productIDsStorage.length > 0 ){
        productIDsStorage.forEach((prod) {
          var product = data.firstWhere((p) => p.id.toString() == prod);
          product.onBasket = true;
        });
      }
    }
    _setProductIDToBasket(int _id) async {
      final prefs = await SharedPreferences.getInstance();

      final myStringList = prefs.getStringList('productsIDInBasket') ?? [];
      
      myStringList.add(_id.toString());

      await prefs.setStringList('productsIDInBasket', myStringList);
    }

    _removeProductFromBasket(int _id) async {
      final prefs = await SharedPreferences.getInstance();

      final myStringList = prefs.getStringList('productsIDInBasket') ?? [];
      
      myStringList.remove(_id.toString());
      
      await prefs.setStringList('productsIDInBasket', myStringList);
    }

    @override
    Widget build(BuildContext context) {
      loadProuctIDInBusket();

      return Container(
        child: Column(
         children: <Widget> [
          Expanded(
            child: ListView.builder(
              itemCount: data.length,
              itemBuilder: (context, i){
                return Card(
                  elevation: 2.0,
                  margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                  child: Container(
                    decoration: BoxDecoration(color: Colors.white),
                    child: ListTile(
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      title: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text(data[i].name),
                        ),
                      subtitle: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(data[i].desc),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(data[i].price.toString() + ' руб.'),
                          ),
                        
                          Padding(padding: EdgeInsets.all(8.0))
                        ],
                        crossAxisAlignment: CrossAxisAlignment.start,
                      ),
                      leading: Image(image: AssetImage(data[i].image), width: 75.0, height: 120.0, fit: BoxFit.contain,),
                      trailing:
                      Checkbox( 
                        value: data[i].onBasket, 
                        onChanged: (bool newValue) {
                          data[i].onBasket ?
                              _removeProductFromBasket(data[i].id)
                            :
                              _setProductIDToBasket(data[i].id);

                          setState(() {
                            data[i].onBasket = newValue;
                          });
                        },
                      ) 
                    )
                  ),
                );
              },
            ),
          ),
          RaisedButton(
            onPressed: (){Navigator.pushNamed(context, '/basket');},
            child: const Text(
              'Перейти в корзину',
              style: TextStyle(fontSize: 20)
            ),
          ),
         ]
        )
      );
      
    }
  }

class OnBasketTarget extends StatefulWidget {
  final bool _onBasket;
  final int _prodID;

  OnBasketTarget(this._onBasket, this._prodID);
  
  @override
  createState() => OnBasketTargetState(_onBasket, _prodID);
}

class OnBasketTargetState extends State<OnBasketTarget>{
  bool onBasket;
  int prodID;


  _setProductIDToBasket(int _id) async {
    final prefs = await SharedPreferences.getInstance();

    final myStringList = prefs.getStringList('productsIDInBasket') ?? [];
    
    myStringList.add(_id.toString());

    await prefs.setStringList('productsIDInBasket', myStringList);
  }

  _removeProductFromBasket(int _id) async {
    final prefs = await SharedPreferences.getInstance();

    final myStringList = prefs.getStringList('productsIDInBasket') ?? [];
    
    myStringList.remove(_id.toString());
    
    await prefs.setStringList('productsIDInBasket', myStringList);
  }
  
  
  OnBasketTargetState(this.onBasket, this.prodID);

  @override
  Widget build(BuildContext context){
  
    return Checkbox( 
        value: onBasket, 
        onChanged: (bool newValue) {
          onBasket ?
              _removeProductFromBasket(prodID)
            :
              _setProductIDToBasket(prodID);

          setState(() {

            onBasket = newValue;

          });
        },
      );
  }
}

