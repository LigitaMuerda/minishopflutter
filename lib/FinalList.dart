import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_begin/ProductListData.dart';
import 'package:flutter_begin/FinalListData.dart';

List<ProductListData> data = [
  ProductListData(id: 1, name: 'Банан', desc: 'Спелый банан', price: 40.0, image: 'assets/images/banan.jpg', onBasket: false),
  ProductListData(id: 2, name: 'Груша', desc: 'Желтая груша', price: 53.0, image: 'assets/images/grusha.jpg', onBasket: false),
  ProductListData(id: 3, name: 'Апельсин', desc: 'Вкусный апельсин', price: 33.0, image: 'assets/images/6XZSr6ddCl6cxfo0UchP.jpg', onBasket: false),
  ProductListData(id: 4, name: 'Мандарин', desc: 'Сладкий мандарин', price: 50.0, image: 'assets/images/mandarin.jpg', onBasket: false),
  ProductListData(id: 5, name: 'Виноград', desc: 'Виноград с косточкой', price: 45.2, image: 'assets/images/dSTg1Wk44PJACMVYH1Z5.jpg', onBasket: false),
  ProductListData(id: 6, name: 'Киви', desc: 'Кислый киви', price: 60.4, image: 'assets/images/114963_900.jpg', onBasket: false),
];

List<FinalListData> dataFinal = [];

double totalPrcieOfBasket = 0; 

class FinalList extends StatefulWidget{
  
  @override
  createState() => FinalListState();
}

class FinalListState extends State<FinalList>{
  
  @override
  Widget build(BuildContext context){
    
    setState(() {
      dataFinal = [];
    });
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        title: Text('Чек'),
      ),
      body: FinalListBuild(),
    );

  } 
}

class FinalListBuild extends StatefulWidget {

  @override
  createState() => FinalListBuildState();
}

class FinalListBuildState extends State<FinalListBuild> {

  _loadProuctIDInBusket() async{
    final prefs = await SharedPreferences.getInstance();
    List<String> productIDsStorage = prefs.getStringList('productsIDInBasket') ?? [];
    
    if(productIDsStorage.length > 0 ){
      productIDsStorage.forEach((prod) {
        var product = data.firstWhere((p) => p.id.toString() == prod);
        
        if(dataFinal.length == 0){
          setState(() {
            dataFinal.add(FinalListData(id: product.id, name: product.name, desc: product.desc, price: product.price, image: product.image, count: 1, totalPrice: 0));
          });
        } else {
          if(dataFinal.length == 1){
            var isProdinBasketALready = dataFinal[0].id.toString() == prod;
            if(isProdinBasketALready == false) {
              setState(() {
                dataFinal.add(FinalListData(id: product.id, name: product.name, desc: product.desc, price: product.price, image: product.image, count: 1, totalPrice: 0));
              });
            }
          } else {
            var isProdinBasketALready = dataFinal.singleWhere((p) => p.id.toString() == prod, orElse: () => null);
            if(isProdinBasketALready == null)
              setState(() {
                dataFinal.add(FinalListData(id: product.id, name: product.name, desc: product.desc, price: product.price, image: product.image, count: 1, totalPrice: 0));
              });
          }  
        } 
      });

      dataFinal.forEach((p) => {
        if(prefs.getDouble(p.id.toString()) != null)
          p.count = prefs.getDouble(p.id.toString())
      });
    }
  }
  
  _totalPriceGet(double _count, double _price){
    double _totalPrice = _count * _price; 
    return _totalPrice.toStringAsFixed(2);
  }

  _getTotalPriceOfBasket() {
    totalPrcieOfBasket = 0;
    if(dataFinal != null){
      dataFinal.forEach((prod) => totalPrcieOfBasket = totalPrcieOfBasket + (prod.count*prod.price) );
    }
    else 
      totalPrcieOfBasket = 0;

    return totalPrcieOfBasket;
  }
  @override
  Widget build(BuildContext context) {  
    _loadProuctIDInBusket();
    return Container(
      child: Column(
        children: <Widget> [
          if(dataFinal.length == 0)
            Expanded(
              child: Center(
                child: Text(
                  'Нет товаров в корзине',
                  style: TextStyle(color: Colors.white, fontSize: 15)
                )
              )
            )
          else
          Expanded(
            child:ListView.builder(
              itemCount: dataFinal.length,
              itemBuilder: (context, i){
                return Card(
                  elevation: 2.0,
                  margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                  child: Container(
                    decoration: BoxDecoration(color: Colors.white),
                    child: ListTile(
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      title: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Text(dataFinal[i].name + ' - ' + dataFinal[i].price.toString() + ' руб.'),
                      ),
                    subtitle: Column(children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Text(dataFinal[i].desc),
                      ),
                      Padding(
                        padding: EdgeInsets.all(4.0),
                        child: Text('Количество: ${dataFinal[i].count.toStringAsFixed(0)}'),
                      ),
                      Padding(
                        padding: EdgeInsets.all(4.0),
                        child: Text('Стоимость: ${_totalPriceGet(dataFinal[i].count, dataFinal[i].price)}'),
                      ),
                    ],
                    crossAxisAlignment: CrossAxisAlignment.start,
                    ),
                    leading: Image(image: AssetImage(dataFinal[i].image), width: 50.0, height: 100.0, fit: BoxFit.contain,),
                    ),
                  )
                );
              },
            )
          ),
          Container(
            height: 55,
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Column(
              children: <Widget>[
                if(_getTotalPriceOfBasket() != 0)
                  Text(
                    'Общая сумма покупок: ${_getTotalPriceOfBasket().toStringAsFixed(2)} руб.',
                    style: TextStyle(color: Colors.white, fontSize: 15),
                  )
                else
                  Text(
                    'В корзине нет покупок',
                    style: TextStyle(color: Colors.white, fontSize: 15),
                  ),
              ],
            )
          )
        ]
      )
    );
  }

}

