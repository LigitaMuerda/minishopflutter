class FinalListData{
  int id;
  String name;
  String desc;
  double price;
  String image;
  double count;
  double totalPrice;
  
  FinalListData({this.id, this.name, this.desc, this.price, this.image, this.count, this.totalPrice});
}