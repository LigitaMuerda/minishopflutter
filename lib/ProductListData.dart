class ProductListData{
  int id;
  String name;
  String desc;
  double price;
  String image;
  bool onBasket;
  
  ProductListData({this.id, this.name, this.desc, this.price, this.image, this. onBasket});
}