import 'package:flutter/material.dart';
import 'package:flutter_begin/ProductList.dart';
import 'package:flutter_begin/BasketList.dart';
import 'package:flutter_begin/FinalList.dart';

void main() => runApp(MainShopPage());

class MainShopPage extends StatelessWidget{

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title: 'Продукты',
      theme: ThemeData(
        primaryColor: Color.fromRGBO(50, 65, 85, 1),
        textTheme: TextTheme(title: TextStyle(color: Colors.white))
      ),
      home: ProductList(), 
      routes: {
        '/home': (BuildContext context) => ProductList(),
        '/basket': (BuildContext context) => BasketList(),
        '/final_list': (BuildContext context) => FinalList(),
      }
    );
  }
}
