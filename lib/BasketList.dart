import 'package:flutter/material.dart';
import 'package:flutter_begin/BasketListData.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_begin/ProductListData.dart';

List<ProductListData> data = [
  ProductListData(id: 1, name: 'Банан', desc: 'Спелый банан', price: 40.0, image: 'assets/images/banan.jpg', onBasket: false),
  ProductListData(id: 2, name: 'Груша', desc: 'Желтая груша', price: 53.0, image: 'assets/images/grusha.jpg', onBasket: false),
  ProductListData(id: 3, name: 'Апельсин', desc: 'Вкусный апельсин', price: 33.0, image: 'assets/images/6XZSr6ddCl6cxfo0UchP.jpg', onBasket: false),
  ProductListData(id: 4, name: 'Мандарин', desc: 'Сладкий мандарин', price: 50.0, image: 'assets/images/mandarin.jpg', onBasket: false),
  ProductListData(id: 5, name: 'Виноград', desc: 'Виноград с косточкой', price: 45.2, image: 'assets/images/dSTg1Wk44PJACMVYH1Z5.jpg', onBasket: false),
  ProductListData(id: 6, name: 'Киви', desc: 'Кислый киви', price: 60.4, image: 'assets/images/114963_900.jpg', onBasket: false),
];

List<BasketListData> dataBasket = [];

double totalPrcieOfBasket = 0; 

class BasketList extends StatefulWidget{
  
  @override
  createState() => BasketListState();
}

class BasketListState extends State<BasketList>{
  
  @override
  Widget build(BuildContext context){
    
    setState(() {
      dataBasket = [];
    });
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        title: Text('Корзина'),
      ),
      body: BasketListBuild(),
    );

  } 
}


class BasketListBuild extends StatefulWidget {

  @override
  createState() => BasketListBuildState();
}

class BasketListBuildState extends State<BasketListBuild> {
  
  _setProductIDToBasket(int _id) async {
    final prefs = await SharedPreferences.getInstance();

    final myStringList = prefs.getStringList('productsIDInBasket') ?? [];
    
    myStringList.add(_id.toString());

    await prefs.setStringList('productsIDInBasket', myStringList);
  }

  _setProductCount(int _id, double _count) async {
    final prefs = await SharedPreferences.getInstance();
  
    prefs.setDouble(_id.toString(), _count);
  }

  _removeProductFromBasket(int _id) async {
    final prefs = await SharedPreferences.getInstance();

    final myStringList = prefs.getStringList('productsIDInBasket') ?? [];
    
    myStringList.remove(_id.toString());
    
    await prefs.setStringList('productsIDInBasket', myStringList);

    final _product = dataBasket.firstWhere((p) => p.id == _id);

    setState(() {
      dataBasket.remove(_product);
    });

  }

  _loadProuctIDInBusket() async{
    final prefs = await SharedPreferences.getInstance();
    List<String> productIDsStorage = prefs.getStringList('productsIDInBasket') ?? [];

    if(productIDsStorage.length > 0 ){
      productIDsStorage.forEach((prod) {
        var product = data.firstWhere((p) => p.id.toString() == prod);

        if(dataBasket.length == 0){
          setState(() {
            dataBasket.add(BasketListData(id: product.id, name: product.name, desc: product.desc, price: product.price, image: product.image, count: 1, totalPrice: 0));
          });
        } else {
          if(dataBasket.length == 1){
            var isProdinBasketALready = dataBasket[0].id.toString() == prod;
            if(isProdinBasketALready == false) {
              setState(() {
                dataBasket.add(BasketListData(id: product.id, name: product.name, desc: product.desc, price: product.price, image: product.image, count: 1, totalPrice: 0));
              });
            }
          } else {
            var isProdinBasketALready = dataBasket.singleWhere((p) => p.id.toString() == prod, orElse: () => null);
            if(isProdinBasketALready == null)
              setState(() {
                dataBasket.add(BasketListData(id: product.id, name: product.name, desc: product.desc, price: product.price, image: product.image, count: 1, totalPrice: 0));
              });
          }  
        } 

        dataBasket.forEach((p) => {
          if(prefs.getDouble(p.id.toString()) != null)
            p.count = prefs.getDouble(p.id.toString())
        });
        
      });
    }
  }
  
  _totalPriceGet(double _count, double _price){
    double _totalPrice = _count * _price; 
    return _totalPrice.toStringAsFixed(2);
  }

  _getTotalPriceOfBasket() {
    totalPrcieOfBasket = 0;
    if(dataBasket != null){
      dataBasket.forEach((prod) => totalPrcieOfBasket = totalPrcieOfBasket + (prod.count*prod.price) );
    }
    else 
      totalPrcieOfBasket = 0;

    return totalPrcieOfBasket;
  }
  @override
  Widget build(BuildContext context) {  
    _loadProuctIDInBusket();
    return Container(
      child: Column(
        children: <Widget> [
          if(dataBasket.length == 0)
            Expanded(
              child: Center(
                child: Text(
                  'Нет товаров в корзине',
                  style: TextStyle(color: Colors.white, fontSize: 15)
                )
              )
            )
          else
          Expanded(
            child:ListView.builder(
              itemCount: dataBasket.length,
              itemBuilder: (context, i){
                return Card(
                  elevation: 2.0,
                  margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                  child: Container(
                    decoration: BoxDecoration(color: Colors.white),
                    child: ListTile(
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      title: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Text(dataBasket[i].name + ' - ' + dataBasket[i].price.toString() + ' руб.'),
                      ),
                    subtitle: Column(children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Text(dataBasket[i].desc),
                      ),
                      Padding(
                        padding: EdgeInsets.all(4.0),
                        child: Text('Количество: ${dataBasket[i].count.toStringAsFixed(0)}'),
                      ),
                      Padding(
                        padding: EdgeInsets.all(4.0),
                        child: Text('Стоимость: ${_totalPriceGet(dataBasket[i].count, dataBasket[i].price)}'),
                      ),
                      Slider(
                        value: dataBasket[i].count, 
                        onChanged: (newVal) => {
                          setState(() => dataBasket[i].count = newVal),
                          _setProductCount(dataBasket[i].id, newVal)
                        },
                        divisions: 9,
                        min: 1.0,
                        max: 10.0,
                        )
                    ],
                    crossAxisAlignment: CrossAxisAlignment.start,
                    ),
                    leading: Image(image: AssetImage(dataBasket[i].image), width: 50.0, height: 100.0, fit: BoxFit.contain,),
                    trailing:
                    SizedBox(
                        height: 30.0,
                        width: 30.0,
                        child: IconButton(
                        onPressed: () => _removeProductFromBasket(dataBasket[i].id),
                        icon: Icon(Icons.restore_from_trash, color: Colors.grey,),
                      ),
                      )
                    ),
                  )
                );
              },
            )
          ),
          Container(
            height: 95,
            padding: EdgeInsets.symmetric(vertical: 10),
            child: Column(
              children: <Widget>[
                if(_getTotalPriceOfBasket() != 0)
                  Text(
                    'Общая сумма покупок: ${_getTotalPriceOfBasket().toStringAsFixed(2)} руб.',
                    style: TextStyle(color: Colors.white, fontSize: 15),
                  )
                else
                  Text(
                    'В корзине нет покупок',
                    style: TextStyle(color: Colors.white, fontSize: 15),
                  ),
                RaisedButton(
                  onPressed: (){Navigator.pushNamed(context, '/final_list');},
                  child: const Text(
                    'Купить',
                    style: TextStyle(fontSize: 20)
                  ),
                ),
              ],
            )
          )
        ]
      )
    );
  }

}

