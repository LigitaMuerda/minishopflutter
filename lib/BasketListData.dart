class BasketListData{
  int id;
  String name;
  String desc;
  double price;
  String image;
  double count;
  double totalPrice;
  
  BasketListData({this.id, this.name, this.desc, this.price, this.image, this.count, this.totalPrice});
}